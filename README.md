# Agenda-MV (FRONT)

Projeto de uma agenda de contatos, com dados pessoais e telefones.

## Rodando a Aplicação

verifique se ha instalado em sua maquina **node**

rode `npm install` na raiz do projeto (onde se encontra o arquivo package.json)

no terminal de preferência rode `npm start`. Navegue para `http://localhost:4200/`.

## PROXY Configurado para CORS

 - ao rodar `npm start` será executado `ng serve --proxy-config proxy.config.json`
 - esse proxy.config.json é um arquivo que está na raiz do projeto, que cuidará de fazer o tratamento de requisições para a API
 - OBS: USAR ISSO APENAS EM CASOS DE AMBIENTE TESTE, em produção NÃO.

