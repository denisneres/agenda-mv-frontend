import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { MenuComponent } from './components/menu/menu.component';
import { ListagemPessoaComponent } from './components/listagem-pessoa/listagem-pessoa.component';
import { routes } from './app.routes';
import { FormPessoaComponent } from './components/form-pessoa/form-pessoa.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    ListagemPessoaComponent,
    FormPessoaComponent
  ],
  imports: [
    BrowserModule,
    routes,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
