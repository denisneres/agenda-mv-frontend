import { Routes, RouterModule } from '@angular/router';
import { ListagemPessoaComponent } from './components/listagem-pessoa/listagem-pessoa.component';
import { ModuleWithProviders } from '@angular/compiler/src/core';
import { ListagemPessoaResolver } from './resolvers/listagem-pessoa.resolver';
import { FormPessoaComponent } from './components/form-pessoa/form-pessoa.component';

export const ROUTES: Routes = [

    {path:'', component: ListagemPessoaComponent, resolve:{pessoas: ListagemPessoaResolver} },
    {path:'form-pessoa', component: FormPessoaComponent},
    {path:'form-pessoa/:id', component: FormPessoaComponent}
]

export const routes: ModuleWithProviders = RouterModule.forRoot(ROUTES);