import { Component, OnInit } from '@angular/core';
import { PessoaService } from 'src/app/services/pessoa.service';
import { Pessoa } from 'src/app/models/pessoa.model';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
import { Telefone } from 'src/app/models/telefone.model';

@Component({
  selector: 'app-form-pessoa',
  templateUrl: './form-pessoa.component.html',
  styleUrls: ['./form-pessoa.component.css']
})
export class FormPessoaComponent implements OnInit {

  pessoaForm: FormGroup;
  pessoa: Pessoa = new Pessoa();
  ddd: string = '';
  numero: string = '';
  telefones: Telefone[] = [];
  


  constructor(private pessoaService: PessoaService,
              private formBuilder: FormBuilder,
              private router: Router,
		          private route: ActivatedRoute, ) { }

  ngOnInit() {
    
    this.pessoaForm = this.formBuilder.group({
			'nome': ['', Validators.required],
			'cpf': ['', Validators.required],
			'dataNascimento': ['', Validators.required],
      'email': ['', Validators.required]
		});

		let id: string = this.route.snapshot.params.id;
		if (id != undefined) {
			this.pessoaService.findById(id)
				.subscribe((res: Pessoa) => {
				
          this.pessoa = res;
					this.populateForm(this.pessoa);
				}, e => this.router.navigate(['/taskNotFound']));
		} else {
      
    }
	}

  populateForm(pessoa: Pessoa): void {
		this.pessoaForm.patchValue({
			nome: pessoa.nome,
			cpf: pessoa.cpf,
			dataNascimento: pessoa.dataNascimento,
      email: pessoa.email
    });
    this.telefones = pessoa.telefones;
	}

  salvarPessoa() {
    const submittedPessoa = this.pessoaForm.getRawValue() as Pessoa;
    submittedPessoa.id = this.pessoa.id;
    submittedPessoa.telefones = this.telefones;
    
    console.log(submittedPessoa);
		this.pessoaService.createOrUpdate(submittedPessoa)
			.subscribe((res: Pessoa) => {
				this.router.navigate(['/']);
			});
  }

  cadastrarNumero() {
    
    this.telefones.push(new Telefone(null,this.ddd,this.numero));
    this.ddd = '';
    this.numero = '';
  }

  removerTelefone(telefone: Telefone) {
    const index: number = this.telefones.indexOf(telefone);
    if (index !== -1) {
      this.telefones.splice(index, 1);
  }      
  }
}
