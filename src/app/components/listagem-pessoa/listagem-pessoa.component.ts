import { Component, OnInit } from '@angular/core';
import { Pessoa } from 'src/app/models/pessoa.model';
import { ActivatedRoute, Router } from '@angular/router';
import { PessoaService } from 'src/app/services/pessoa.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-listagem-pessoa',
  templateUrl: './listagem-pessoa.component.html',
  styleUrls: ['./listagem-pessoa.component.css']
})
export class ListagemPessoaComponent implements OnInit {

  pessoas: Pessoa[];

  constructor(private activateRoute: ActivatedRoute, 
              private pessoaService: PessoaService,
              private router: Router) { }

  ngOnInit() {
    this.pessoas = this.activateRoute.snapshot.data.pessoas;
  }

  removerPessoa(id:string) {
    this.pessoaService.delete(id).subscribe( msg => {
      console.log(msg);
      this.updateListaPessoas();
    }, err => {
      this.updateListaPessoas();
    });
  }

  updateListaPessoas() {
    this.pessoaService.findAll().subscribe((pessoas: Pessoa[]) => {
      console.log(pessoas);
      this.pessoas = pessoas;
    });
  }
}
