import { Telefone } from './telefone.model';

export class Pessoa {
    public id: number;
    public nome: string;
    public cpf: String;
    public dataNascimento: Date;
    public email: string;
    public telefones: Telefone[] = [];
    constructor(){}
}