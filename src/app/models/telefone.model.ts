import { Pessoa } from './pessoa.model';

export class Telefone {
    constructor(
        public id: number,
        public ddd: string,
        public numero: string
    ){}
}