import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import { Pessoa } from '../models/pessoa.model';
import { PessoaService } from '../services/pessoa.service';

@Injectable({
	providedIn: 'root'
})
export class ListagemPessoaResolver implements Resolve<Observable<Pessoa>> {

	constructor(private pessoaService: PessoaService) {}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Pessoa> {
		return this.pessoaService.findAll() as any;
	}
}
