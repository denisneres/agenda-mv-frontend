import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Pessoa } from '../models/pessoa.model';
import { AGENDAMV_API } from './agendamv.api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PessoaService {

  constructor(private _http: HttpClient) { }


  createOrUpdate(pessoa:Pessoa) {
    if(pessoa.id != null) {
      return this._http.put(`${AGENDAMV_API}/pessoa`, pessoa); 
    } else {
      pessoa.id = null;
      return this._http.post(`${AGENDAMV_API}/pessoa`, pessoa);
    }
  }

  findAll() {
    return this._http.get(`${AGENDAMV_API}/pessoa`);
  }

  findById(id: string) {
    return this._http.get(`${AGENDAMV_API}/pessoa/${id}`);
  }

  delete(id: string) {
    return this._http.delete(`${AGENDAMV_API}/pessoa/${id}`);
  }
}
